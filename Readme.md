# Exercicio 1

O Exercício está localizado dentro da pasta Exercicio1/, e é facilmente executado através do visual studio ou dotnet cli

# Exercicio 2

O Exercicio 2 está localizado dentro da pasta Exercicio2/, e é necessário ter a versão mais recente do angular cli (6 no momento). Para executar o projeto execute `ng serve`.

# Exercicio 3

O Exercício 3 está localizado dentro da parta Exercicio3/, ele é composto pelo BackEnd (Exercicio3.WebApi) e o frontend (Exercicio3.WebApp), os dois são executados separadamente.
Para executar o BackEnd, você deve ter instalado o [Docker for Windows](https://docs.docker.com/docker-for-windows/install/) e executar o projeto docker-compose no Visual Studio, 
isso fará com que seja provisionado um container para a aplicação e outro container para o Sql Server for Linux.

## Criando a Base de dados

O container é provisionado sem nenhuma base de dados, para isso crie a base de dados acessando por linha de comando o container:

`docker container exec -it exercicio3.db bash`

Após isso execute:

`/opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P Exercicio@3`

Agora você pode executar o comando para criar a base:

`Create Database Exercicio3`

`GO`

## Executando as migrations

Através do Package Manager Console com o projeto Exercicio3.Infra Selecionado como "Startup Project", execute:

`Update-Database`

Ou através do dotnet cli, navegue até a pasta src/Exercicio3.Infra e execute:

`dotnet ef database update`

## Verificando Configurações de ambiente no frontend

Dentro da Pasta Exercicio3/src/Exercicio3.WebApp que contém o frontend do projeto existe a configuração do endereço e porta da api (src/environments/environment.ts), verifique se está de acordo com a aplicação rodandod no visual studio.

## Arquitetura e design da solução

No Backend da solução Exercicio3, foram empregados alguns conceitos sucintos de Domain Driven Design, como Entidades, Value Objects, separação do domínio da lógica de Infraestrutura, etc.
Além disso alguns outros conceitos e patterns estão presentes na solução, como Unit Of Work, Repository Pattern e Notification Pattern. Para persistência foi utilizado o Entity Framework Core, que 
de acordo com os requisitos do projeto atendeu bem. Para operações CRUD como Vagas e Tecnologias, foi empregada um fluxo simplificado, pois a complexidade é baixa, utilizando somente os Controllers, ViewModels, Entidades e Repositórios. 
já na funcionalidade de entrevista e relatório de seleção foi utilizado uma estrutura diferente, com alguns conceitos iniciais de CQRS (Command Query Responsability Segregation) pois haviam mais regras a serem implementadas, 
e manter essa lógica no controller iria prejudicar a legibilidade e ferir conceitos de separação de responsabilidades.
Acredito que posso ter cometido um erro de design ao colocar a funcionalidade de relatório de seleção como um command do CQRS, pois, Commands são denominados aqueles que alteram o estado a aplicação, porém, não pensei em uma melhor abordagem e não havia muito tempo.