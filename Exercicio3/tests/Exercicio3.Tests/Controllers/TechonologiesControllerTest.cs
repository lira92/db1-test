﻿using Exercicio3.Domain.Entities;
using Exercicio3.Domain.Repositories;
using Exercicio3.Infra.Transaction;
using Exercicio3.WebApi.Controllers;
using Exercicio3.WebApi.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Exercicio3.Tests.Controllers
{
    [TestClass]
    public class TechonologiesControllerTest
    {
        [TestMethod]
        [TestCategory("Techonology - Create")]
        public async Task ShouldReturnSuccessWhenTechnologyIsValid()
        {
            var mockIUow = new Mock<IUow>();
            var mockLogger = new Mock<ILogger<BaseController>>();

            var mockTechnologyRepo = new Mock<ITechnologyRepository>();

            var controller = new TechnologiesController(mockIUow.Object, mockLogger.Object, AutoMapperHelper.GetMapper(), mockTechnologyRepo.Object);

            var result = await controller.CreateTechnology(new TechnologyViewModel()
            {
                Name = "test technology"
            });

            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.AreEqual(((OkObjectResult)result).StatusCode, 200);
            mockTechnologyRepo.Verify(x => x.Create(It.IsAny<Technology>()), Times.Once());
        }

        [TestMethod]
        [TestCategory("Techonology - Create")]
        public async Task ShouldReturnBadRequestWhenTechnologyIsNotValid()
        {
            var mockIUow = new Mock<IUow>();
            var mockLogger = new Mock<ILogger<BaseController>>();

            var mockTechnologyRepo = new Mock<ITechnologyRepository>();

            var controller = new TechnologiesController(mockIUow.Object, mockLogger.Object, AutoMapperHelper.GetMapper(), mockTechnologyRepo.Object);

            var result = await controller.CreateTechnology(new TechnologyViewModel()
            {
                Name = ""
            });

            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
            Assert.AreEqual(((BadRequestObjectResult)result).StatusCode, 400);
            mockTechnologyRepo.Verify(x => x.Create(It.IsAny<Technology>()), Times.Never());
        }

        [TestMethod]
        [TestCategory("Techonology - Update")]
        public async Task ShouldReturnSuccessOnUpdateWhenTechnologyIsValid()
        {
            var mockIUow = new Mock<IUow>();
            var mockLogger = new Mock<ILogger<BaseController>>();

            var mockTechnologyRepo = new Mock<ITechnologyRepository>();
            var technologyId = Guid.NewGuid();
            var entity = new Technology("technology test");
            mockTechnologyRepo.Setup(x => x.Get(technologyId)).Returns(entity);

            var controller = new TechnologiesController(mockIUow.Object, mockLogger.Object, AutoMapperHelper.GetMapper(), mockTechnologyRepo.Object);

            var result = await controller.UpdateTechnology(technologyId, new TechnologyViewModel()
            {
                Name = "technology test changed"
            });

            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.AreEqual(((OkObjectResult)result).StatusCode, 200);
            Assert.AreEqual(entity.Name, "technology test changed");
            Assert.IsNotNull(entity.UpdatedAt);
            mockTechnologyRepo.Verify(x => x.Update(It.IsAny<Technology>()), Times.Once());
        }

        [TestMethod]
        [TestCategory("Techonology - Update")]
        public async Task ShouldReturnBadRequestOnUpdateWhenTechnologyIsNotValid()
        {
            var mockIUow = new Mock<IUow>();
            var mockLogger = new Mock<ILogger<BaseController>>();

            var mockTechnologyRepo = new Mock<ITechnologyRepository>();
            var technologyId = Guid.NewGuid();
            var entity = new Technology("technology test");
            mockTechnologyRepo.Setup(x => x.Get(technologyId)).Returns(entity);

            var controller = new TechnologiesController(mockIUow.Object, mockLogger.Object, AutoMapperHelper.GetMapper(), mockTechnologyRepo.Object);

            var result = await controller.UpdateTechnology(technologyId, new TechnologyViewModel()
            {
                Name = "",
            });

            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
            Assert.AreEqual(((BadRequestObjectResult)result).StatusCode, 400);
            mockTechnologyRepo.Verify(x => x.Update(It.IsAny<Technology>()), Times.Never());
        }

        [TestMethod]
        [TestCategory("Techonology - Remove")]
        public async Task ShouldReturnSuccessOnRemoveTechnology()
        {
            var mockIUow = new Mock<IUow>();
            var mockLogger = new Mock<ILogger<BaseController>>();

            var mockTechnologyRepo = new Mock<ITechnologyRepository>();
            var technologyId = Guid.NewGuid();
            var entity = new Technology("technology test");
            mockTechnologyRepo.Setup(x => x.Get(technologyId)).Returns(entity);

            var controller = new TechnologiesController(mockIUow.Object, mockLogger.Object, AutoMapperHelper.GetMapper(), mockTechnologyRepo.Object);

            var result = await controller.RemoveTechnology(technologyId);

            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.AreEqual(((OkObjectResult)result).StatusCode, 200);
            mockTechnologyRepo.Verify(x => x.Remove(It.IsAny<Technology>()), Times.Once());
        }
    }
}
