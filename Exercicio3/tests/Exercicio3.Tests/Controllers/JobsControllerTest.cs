﻿using AutoMapper;
using Exercicio3.Domain.Entities;
using Exercicio3.Domain.Repositories;
using Exercicio3.Infra.Transaction;
using Exercicio3.WebApi.Controllers;
using Exercicio3.WebApi.Mappers;
using Exercicio3.WebApi.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace Exercicio3.Tests.Controllers
{
    [TestClass]
    public class JobsControllerTest
    {
        [TestMethod]
        [TestCategory("Job - Create")]
        public async Task ShouldReturnSuccessWhenJobIsValid()
        {
            var mockIUow = new Mock<IUow>();
            var mockLogger = new Mock<ILogger<BaseController>>();

            var mockJobRepository = new Mock<IJobRepository>();

            var controller = new JobsController(mockIUow.Object, mockLogger.Object, AutoMapperHelper.GetMapper(), mockJobRepository.Object);

            var result = await controller.CreateJob(new JobViewModel()
            {
                Name = "test job",
                Description = "job description"
            });

            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.AreEqual(((OkObjectResult)result).StatusCode, 200);
            mockJobRepository.Verify(x => x.Create(It.IsAny<Job>()), Times.Once());
        }

        [TestMethod]
        [TestCategory("Job - Create")]
        public async Task ShouldReturnBadRequestWhenJobIsNotValid()
        {
            var mockIUow = new Mock<IUow>();
            var mockLogger = new Mock<ILogger<BaseController>>();

            var mockJobRepository = new Mock<IJobRepository>();

            var controller = new JobsController(mockIUow.Object, mockLogger.Object, AutoMapperHelper.GetMapper(), mockJobRepository.Object);

            var result = await controller.CreateJob(new JobViewModel()
            {
                Name = "",
                Description = "job description"
            });

            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
            Assert.AreEqual(((BadRequestObjectResult)result).StatusCode, 400);
            mockJobRepository.Verify(x => x.Create(It.IsAny<Job>()), Times.Never());
        }

        [TestMethod]
        [TestCategory("Job - Update")]
        public async Task ShouldReturnSuccessOnUpdateWhenJobIsValid()
        {
            var mockIUow = new Mock<IUow>();
            var mockLogger = new Mock<ILogger<BaseController>>();

            var mockJobRepository = new Mock<IJobRepository>();
            var jobId = Guid.NewGuid();
            var entity = new Job("job test", "job description");
            mockJobRepository.Setup(x => x.Get(jobId)).Returns(entity);

            var controller = new JobsController(mockIUow.Object, mockLogger.Object, AutoMapperHelper.GetMapper(), mockJobRepository.Object);

            var result = await controller.UpdateJob(jobId, new JobViewModel()
            {
                Name = "job test changed",
                Description = "job description changed"
            });

            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.AreEqual(((OkObjectResult)result).StatusCode, 200);
            Assert.AreEqual(entity.Name, "job test changed");
            Assert.AreEqual(entity.Description, "job description changed");
            Assert.IsNotNull(entity.UpdatedAt);
            mockJobRepository.Verify(x => x.Update(It.IsAny<Job>()), Times.Once());
        }

        [TestMethod]
        [TestCategory("Job - Update")]
        public async Task ShouldReturnBadRequestOnUpdateWhenJobIsNotValid()
        {
            var mockIUow = new Mock<IUow>();
            var mockLogger = new Mock<ILogger<BaseController>>();

            var mockJobRepository = new Mock<IJobRepository>();
            var jobId = Guid.NewGuid();
            var entity = new Job("job test", "job description");
            mockJobRepository.Setup(x => x.Get(jobId)).Returns(entity);

            var controller = new JobsController(mockIUow.Object, mockLogger.Object, AutoMapperHelper.GetMapper(), mockJobRepository.Object);

            var result = await controller.UpdateJob(jobId, new JobViewModel()
            {
                Name = "",
                Description = "job description changed"
            });

            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
            Assert.AreEqual(((BadRequestObjectResult)result).StatusCode, 400);
            mockJobRepository.Verify(x => x.Update(It.IsAny<Job>()), Times.Never());
        }

        [TestMethod]
        [TestCategory("Job - Remove")]
        public async Task ShouldReturnSuccessOnRemoveJob()
        {
            var mockIUow = new Mock<IUow>();
            var mockLogger = new Mock<ILogger<BaseController>>();

            var mockJobRepository = new Mock<IJobRepository>();
            var jobId = Guid.NewGuid();
            var entity = new Job("job test", "job description");
            mockJobRepository.Setup(x => x.Get(jobId)).Returns(entity);

            var controller = new JobsController(mockIUow.Object, mockLogger.Object, AutoMapperHelper.GetMapper(), mockJobRepository.Object);

            var result = await controller.RemoveJob(jobId);

            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            Assert.AreEqual(((OkObjectResult)result).StatusCode, 200);
            mockJobRepository.Verify(x => x.Remove(It.IsAny<Job>()), Times.Once());
        }
    }
}
