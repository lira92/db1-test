﻿using Exercicio3.Domain.Commands.Handlers;
using Exercicio3.Domain.Commands.Inputs;
using Exercicio3.Domain.Commands.Results;
using Exercicio3.Domain.Entities;
using Exercicio3.Domain.Repositories;
using Exercicio3.Domain.ValueObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exercicio3.Tests.CommandHandlers
{
    [TestClass]
    public class JobRankingCommandHandlerTest
    {
        [TestMethod]
        [TestCategory("Job ranking")]
        public void ShouldCreateCandidateWhenIsValid()
        {
            var job = new Job("job test", "job test description");
            var firstTechnology = new Technology("technology 1");
            var secondTechnology = new Technology("technology 2");

            var firstCandidate = new Candidate("First Candidate", new Email("test@test.com"), job);
            firstCandidate.AddCandidateTechnology(new CandidateTechnology(firstTechnology));
            firstCandidate.AddCandidateTechnology(new CandidateTechnology(secondTechnology));

            var secondCandidate = new Candidate("Second Candidate", new Email("test1@test.com"), job);
            secondCandidate.AddCandidateTechnology(new CandidateTechnology(firstTechnology));

            var mockCandidateRepository = new Mock<ICandidateRepository>();
            mockCandidateRepository.Setup(x => x.GetCandidatesByJob(job.Id)).Returns(new List<Candidate>() { firstCandidate, secondCandidate });

            var handler = new JobRankingCommandHandler(mockCandidateRepository.Object);
            var result = handler.Handle(new JobRankingCommand
            {
                JobId = job.Id,
                Technologies = new List<JobRankingTechnologyCommand>()
                {
                    new JobRankingTechnologyCommand
                    {
                        TechnologyId = firstTechnology.Id,
                        Value = 10
                    },
                    new JobRankingTechnologyCommand
                    {
                        TechnologyId = secondTechnology.Id,
                        Value = 15
                    }
                }
            });

            Assert.IsTrue(handler.Valid);
            Assert.IsInstanceOfType(result, typeof(JobRankingCommandResult));
            Assert.AreEqual(((JobRankingCommandResult)result).Candidates.First().Id, firstCandidate.Id);
            Assert.AreEqual(25, ((JobRankingCommandResult)result).Candidates.First().Score);
            Assert.AreEqual(((JobRankingCommandResult)result).Candidates.Last().Id, secondCandidate.Id);
            Assert.AreEqual(10, ((JobRankingCommandResult)result).Candidates.Last().Score);
        }
    }
}
