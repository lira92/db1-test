﻿using Exercicio3.Domain.Commands.Handlers;
using Exercicio3.Domain.Commands.Inputs;
using Exercicio3.Domain.Entities;
using Exercicio3.Domain.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Exercicio3.Tests.CommandHandlers
{
    [TestClass]
    public class InterviewCommandHandlerTest
    {
        [TestMethod]
        [TestCategory("Interview")]
        public void ShouldCreateCandidateWhenIsValid()
        {
            var mockJobRepository = new Mock<IJobRepository>();
            var mockTechnologyRepository = new Mock<ITechnologyRepository>();
            var mockCandidateRepository = new Mock<ICandidateRepository>();

            var jobId = Guid.NewGuid();
            var firstTechnologyId = Guid.NewGuid();
            var secondTechnologyId = Guid.NewGuid();

            var entity = new Job("job test", "job description");
            mockJobRepository.Setup(x => x.Get(jobId)).Returns(entity);

            var firstTechEntity = new Technology("first technology test");
            mockTechnologyRepository.Setup(x => x.Get(firstTechnologyId)).Returns(firstTechEntity);

            var secondTechEntity = new Technology("second technology test");
            mockTechnologyRepository.Setup(x => x.Get(secondTechnologyId)).Returns(secondTechEntity);

            var handler = new InterviewCommandHandler(mockTechnologyRepository.Object, mockJobRepository.Object, mockCandidateRepository.Object);
            handler.Handle(new InterviewCommand
            {
                Name = "Candidate test",
                Email = "test@test.com",
                JobId = jobId,
                Technologies = new List<Guid>()
                {
                    firstTechnologyId,
                    secondTechnologyId
                }
            });

            Assert.IsTrue(handler.Valid);
            mockCandidateRepository.Verify(x => x.Create(It.IsAny<Candidate>()), Times.Once());
        }

        [TestMethod]
        [TestCategory("Interview")]
        public void ShouldReturnErrorWhenJobNotExists()
        {
            var mockJobRepository = new Mock<IJobRepository>();
            var mockTechnologyRepository = new Mock<ITechnologyRepository>();
            var mockCandidateRepository = new Mock<ICandidateRepository>();

            var jobId = Guid.NewGuid();
            var firstTechnologyId = Guid.NewGuid();
            var secondTechnologyId = Guid.NewGuid();

            var entity = new Job("job test", "job description");
            mockJobRepository.Setup(x => x.Get(jobId)).Returns(entity);

            var firstTechEntity = new Technology("first technology test");
            mockTechnologyRepository.Setup(x => x.Get(firstTechnologyId)).Returns(firstTechEntity);

            var secondTechEntity = new Technology("second technology test");
            mockTechnologyRepository.Setup(x => x.Get(secondTechnologyId)).Returns(secondTechEntity);

            var handler = new InterviewCommandHandler(mockTechnologyRepository.Object, mockJobRepository.Object, mockCandidateRepository.Object);
            handler.Handle(new InterviewCommand
            {
                Name = "Candidate test",
                Email = "test@test.com",
                JobId = Guid.NewGuid(),
                Technologies = new List<Guid>()
                {
                    firstTechnologyId,
                    secondTechnologyId
                }
            });

            Assert.IsFalse(handler.Valid);
            mockCandidateRepository.Verify(x => x.Create(It.IsAny<Candidate>()), Times.Never());
        }

        [TestMethod]
        [TestCategory("Interview")]
        public void ShouldReturnErrorWhenCandidateIsNotValid()
        {
            var mockJobRepository = new Mock<IJobRepository>();
            var mockTechnologyRepository = new Mock<ITechnologyRepository>();
            var mockCandidateRepository = new Mock<ICandidateRepository>();

            var jobId = Guid.NewGuid();
            var firstTechnologyId = Guid.NewGuid();
            var secondTechnologyId = Guid.NewGuid();

            var entity = new Job("job test", "job description");
            mockJobRepository.Setup(x => x.Get(jobId)).Returns(entity);

            var firstTechEntity = new Technology("first technology test");
            mockTechnologyRepository.Setup(x => x.Get(firstTechnologyId)).Returns(firstTechEntity);

            var secondTechEntity = new Technology("second technology test");
            mockTechnologyRepository.Setup(x => x.Get(secondTechnologyId)).Returns(secondTechEntity);

            var handler = new InterviewCommandHandler(mockTechnologyRepository.Object, mockJobRepository.Object, mockCandidateRepository.Object);
            handler.Handle(new InterviewCommand
            {
                Name = "Candidate test",
                Email = "123",
                JobId = jobId,
                Technologies = new List<Guid>()
                {
                    firstTechnologyId,
                    secondTechnologyId
                }
            });

            Assert.IsFalse(handler.Valid);
            mockCandidateRepository.Verify(x => x.Create(It.IsAny<Candidate>()), Times.Never());
        }
    }
}
