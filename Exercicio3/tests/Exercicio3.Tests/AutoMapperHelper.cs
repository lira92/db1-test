﻿using AutoMapper;
using Exercicio3.WebApi.Mappers;

namespace Exercicio3.Tests
{
    public class AutoMapperHelper
    {
        public static IMapper GetMapper()
        {
            var domainToViewModelProfile = new DomainToViewModelProfile();
            var viewModelToDomainProfile = new ViewModelToDomainProfile();
            var configuration = new MapperConfiguration(cfg => {
                cfg.AddProfile(domainToViewModelProfile);
                cfg.AddProfile(viewModelToDomainProfile);
            });
            var mapper = new Mapper(configuration);

            return mapper;
        }
    }
}
