import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomePageComponent } from './pages/home-page/home-page.component';
import { JobsListComponent } from './pages/jobs/jobs-list/jobs-list.component';
import { TechnologiesListComponent } from './pages/technologies/technologies-list/technologies-list.component';
import { JobsAddComponent } from './pages/jobs/jobs-add/jobs-add.component';
import { JobsEditComponent } from './pages/jobs/jobs-edit/jobs-edit.component';
import { TechnologiesEditComponent } from './pages/technologies/technologies-edit/technologies-edit.component';
import { TechnologiesAddComponent } from './pages/technologies/technologies-add/technologies-add.component';
import { InterviewAddComponent } from './pages/interviews/interview-add/interview-add.component';
import { JobRankingComponent } from './pages/job-ranking/job-ranking.component';

const appRoutes: Routes = [
    { path: '', component: HomePageComponent },
    { path: 'home', component: HomePageComponent },
    { path: 'jobs', component: JobsListComponent },
    { path: 'jobs/add', component: JobsAddComponent },
    { path: 'jobs/edit/:id', component: JobsEditComponent },
    { path: 'technologies', component: TechnologiesListComponent },
    { path: 'technologies/add', component: TechnologiesAddComponent },
    { path: 'technologies/edit/:id', component: TechnologiesEditComponent },
    { path: 'interviews/add', component: InterviewAddComponent },
    { path: 'job-ranking', component: JobRankingComponent },
]

export const RoutingProviders: any[] = [];
export const Routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);