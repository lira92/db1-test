import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

// Root
import { AppComponent } from './app.component';

// Routes
import {Routing, RoutingProviders} from './app.routing';

// Pages
import { HomePageComponent } from './pages/home-page/home-page.component';
import { JobsListComponent } from './pages/jobs/jobs-list/jobs-list.component';
import { TechnologiesListComponent } from './pages/technologies/technologies-list/technologies-list.component';

// components
import { AsideMenuComponent } from './components/shared/aside-menu/aside-menu.component';
import { HeadbarComponent } from './components/shared/headbar/headbar.component';

// Services
import { JobsService } from './services/jobs.service';
import { TechnologiesService } from './services/technologies.service';
import { JobsAddComponent } from './pages/jobs/jobs-add/jobs-add.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JobsEditComponent } from './pages/jobs/jobs-edit/jobs-edit.component';
import { TechnologiesAddComponent } from './pages/technologies/technologies-add/technologies-add.component';
import { TechnologiesEditComponent } from './pages/technologies/technologies-edit/technologies-edit.component';
import { InterviewAddComponent } from './pages/interviews/interview-add/interview-add.component';
import { JobRankingComponent } from './pages/job-ranking/job-ranking.component';
import { JobRankingService } from './services/job-ranking.service';
import { InterviewService } from './services/interview.service';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    AsideMenuComponent,
    HeadbarComponent,
    JobsListComponent,
    TechnologiesListComponent,
    JobsAddComponent,
    JobsEditComponent,
    TechnologiesAddComponent,
    TechnologiesEditComponent,
    InterviewAddComponent,
    JobRankingComponent
  ],
  imports: [
    BrowserModule,
    Routing,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
  ],
  providers: [JobsService, TechnologiesService, JobRankingService, InterviewService],
  bootstrap: [AppComponent]
})
export class AppModule { }
