import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class JobsService {

  constructor(private http:Http) { }

  getJobs() {
    return this.http.get(environment.api_base_url + 'v1/jobs')
      .pipe(
        map((response:Response) => {
          return response.json();
        })
      );
  }

  createJob(data: any) {
    return this.http
      .post(environment.api_base_url + 'v1/jobs', data)
      .pipe(
        map((response:Response) => {
          return response.json();
        })
      );
  }

  updateJob(data: any, jobId: any) {
    return this.http
      .put(environment.api_base_url + `v1/jobs/${jobId}`, data)
      .pipe(
        map((response:Response) => {
          return response.json();
        })
      );
  }

  getJob(jobId: any) {
    return this.http
      .get(environment.api_base_url + `v1/jobs/${jobId}`)
      .pipe(
        map((response:Response) => {
          return response.json();
        })
      );
  }

  removeJob(jobId: any) {
    return this.http
      .delete(environment.api_base_url + `v1/jobs/${jobId}`)
      .pipe(
        map((response:Response) => {
          return response.json();
        })
      );
  }
}
