import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TechnologiesService {

  constructor(private http:Http) { }

  getTechnologies() {
    return this.http.get(environment.api_base_url + 'v1/technologies')
      .pipe(
        map((response:Response) => {
          return response.json();
        })
      );
  }

  createTechnology(data: any) {
    return this.http
      .post(environment.api_base_url + 'v1/technologies', data)
      .pipe(
        map((response:Response) => {
          return response.json();
        })
      );
  }

  updateTechnology(data: any, technologyId: any) {
    return this.http
      .put(environment.api_base_url + `v1/technologies/${technologyId}`, data)
      .pipe(
        map((response:Response) => {
          return response.json();
        })
      );
  }

  getTechnology(technologyId: any) {
    return this.http
      .get(environment.api_base_url + `v1/technologies/${technologyId}`)
      .pipe(
        map((response:Response) => {
          return response.json();
        })
      );
  }

  removeTechnology(technologyId: any) {
    return this.http
      .delete(environment.api_base_url + `v1/technologies/${technologyId}`)
      .pipe(
        map((response:Response) => {
          return response.json();
        })
      );
  }
}
