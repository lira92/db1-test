import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InterviewService {

  constructor(private http:Http) { }

  createInterview(data: any) {
    return this.http
      .post(environment.api_base_url + 'v1/interviews', data)
      .pipe(
        map((response:Response) => {
          return response.json();
        })
      );
  }
}
