import { Component, OnInit } from '@angular/core';
import { TechnologiesService } from '../../../services/technologies.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-technologies-edit',
  templateUrl: './technologies-edit.component.html',
  styleUrls: ['./technologies-edit.component.css']
})
export class TechnologiesEditComponent implements OnInit {

  public technologyId: any;
  public form: FormGroup;
  public errors: any[] = [];
  private isLoading: Boolean = false;

  constructor(private fb: FormBuilder, private router: Router, private service: TechnologiesService, private route: ActivatedRoute) { 
    this.form = this.fb.group({
      name: ['', Validators.compose([
        Validators.minLength(1),
        Validators.maxLength(100),
        Validators.required
      ])]
    });
  }

  ngOnInit() {
    this.route.params.subscribe(
      (params: any) => {
        this.technologyId = params['id'];
        this.getTechnology();
      }
    );
  }

  getTechnology() {
    this.service.getTechnology(this.technologyId).subscribe(result => {
      this.form.setValue({
        name: result.data['name']
      });
    }, error => {
      this.errors = [{message: 'Não foi possível recuperar as informações da tecnologia'}];
    });
  }

  submit() {
    this.isLoading = true;
    this.errors = [];
    this.service.updateTechnology(this.form.value, this.technologyId).subscribe(result => {
      this.isLoading = false;
      this.form.reset();
      this.router.navigate(['/technologies']);
    }, error => {
      this.isLoading = false;
      this.errors = JSON.parse(error._body).errors;
    });
  }
}
