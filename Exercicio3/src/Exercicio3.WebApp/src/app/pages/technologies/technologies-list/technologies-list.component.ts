import { Component, OnInit } from '@angular/core';
import { TechnologiesService } from '../../../services/technologies.service';
import * as moment from 'moment';

@Component({
  selector: 'app-technologies-list',
  templateUrl: './technologies-list.component.html',
  styleUrls: ['./technologies-list.component.css']
})
export class TechnologiesListComponent implements OnInit {

  public technologies: any[] = [];
  public errors: any[] = [];
  public success: string;

  constructor(private service:TechnologiesService) { }

  ngOnInit() {
    this.getTechnologies();
  }

  getTechnologies() {
    this.service.getTechnologies().subscribe(result => {
      this.technologies = result.data;
    })
  }

  formatDate(date: any) {
    if(!date) {
      return '';
    }
    return moment(date).format('DD/MM/YYYY HH:mm');
  }

  removeTechnology(id: any) {
    let confirm = window.confirm("Tem certeza que deseja remover essa tecnologia?");
    if(confirm) {
      this.service.removeTechnology(id).subscribe(result => {
        if(result.success) {
          this.success = "Tecnologia removida com sucesso";
          this.getTechnologies();
          return;
        }
        this.errors = result.errors;
      });
    }
  }
}
