import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TechnologiesService } from '../../../services/technologies.service';

@Component({
  selector: 'app-technologies-add',
  templateUrl: './technologies-add.component.html',
  styleUrls: ['./technologies-add.component.css']
})
export class TechnologiesAddComponent implements OnInit {

  public form: FormGroup;
  public errors: any[] = [];
  private isLoading: Boolean = false;
  constructor(private fb: FormBuilder, private router: Router, private service: TechnologiesService) {
    this.form = this.fb.group({
      name: ['', Validators.compose([
        Validators.minLength(1),
        Validators.maxLength(100),
        Validators.required
      ])]
    });
   }

  ngOnInit() {
  }

  submit() {
    this.isLoading = true;
    this.errors = [];
    this.service.createTechnology(this.form.value).subscribe(result => {
      this.isLoading = false;
      this.form.reset();
      this.router.navigate(['/technologies']);
    }, error => {
      this.isLoading = false;
      this.errors = JSON.parse(error._body).errors;
    });
  }
}
