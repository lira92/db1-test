import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { InterviewService } from '../../../services/interview.service';
import { JobsService } from '../../../services/jobs.service';
import { TechnologiesService } from '../../../services/technologies.service';

@Component({
  selector: 'app-interview-add',
  templateUrl: './interview-add.component.html',
  styleUrls: ['./interview-add.component.css']
})
export class InterviewAddComponent implements OnInit {

  public form: FormGroup;
  public errors: any[] = [];
  private isLoading: Boolean = false;
  public jobs: any[] = [];
  public technologies: any[] = [];
  public success: string = "";

  constructor(private fb: FormBuilder, private router: Router, private service: InterviewService, 
    private jobsService: JobsService, private technologiesService:TechnologiesService) {
    this.form = this.fb.group({
      name: ['', Validators.compose([
        Validators.minLength(5),
        Validators.maxLength(100),
        Validators.required
      ])],
      email: ['', Validators.compose([
        Validators.minLength(5),
        Validators.maxLength(160),
        Validators.required
      ])],
      jobId: ['', Validators.compose([
        Validators.required
      ])],
      technologies: this.buildTechnologies()
    });
  }

  ngOnInit() {
    this.getJobs();
    this.getTechnologies();
  }

  getJobs() {
    this.jobsService.getJobs().subscribe(result => {
      this.jobs = result.data;
    })
  }

  getTechnologies() {
    this.technologiesService.getTechnologies().subscribe(result => {
      this.technologies = result.data;
      this.form = this.fb.group({
        name: ['', Validators.compose([
          Validators.minLength(5),
          Validators.maxLength(100),
          Validators.required
        ])],
        email: ['', Validators.compose([
          Validators.minLength(5),
          Validators.maxLength(160),
          Validators.required
        ])],
        jobId: ['', Validators.compose([
          Validators.required
        ])],
        technologies: this.buildTechnologies()
      });
    })
  }

  buildTechnologies() {
    const arr = this.technologies.map(skill => {
      return this.fb.control(false);
    });
    return this.fb.array(arr);
  }

  get technologiesFields() {
    return this.form.get('technologies');
  };

  submit() {
    this.success = "";
    const formValue = Object.assign({}, this.form.value, {
      technologies: this.form.value.technologies
        .map((selected, i) => {
          return {
            id: this.technologies[i].id,
            selected: selected
          }
        })
        .filter((item) => {
          return item.selected;
        })
        .map((item) => {
          return item.id
        })
    });
    this.isLoading = true;
    this.errors = [];
    this.service.createInterview(formValue).subscribe(result => {
      this.isLoading = false;
      this.form.reset();
      this.success = "Entrevista registrada com sucesso";
    }, error => {
      this.isLoading = false;
      this.errors = JSON.parse(error._body).errors;
    });
  }
}
