import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { JobsService } from '../../../services/jobs.service';

@Component({
  selector: 'app-jobs-add',
  templateUrl: './jobs-add.component.html',
  styleUrls: ['./jobs-add.component.css']
})
export class JobsAddComponent implements OnInit {

  public form: FormGroup;
  public errors: any[] = [];
  private isLoading: Boolean = false;
  constructor(private fb: FormBuilder, private router: Router, private service: JobsService) { 
    this.form = this.fb.group({
      name: ['', Validators.compose([
        Validators.minLength(5),
        Validators.maxLength(100),
        Validators.required
      ])],
      description: ['', Validators.compose([
        
      ])]
    });

  }

  ngOnInit() {
  }

  submit() {
    this.isLoading = true;
    this.errors = [];
    this.service.createJob(this.form.value).subscribe(result => {
      this.isLoading = false;
      this.form.reset();
      this.router.navigate(['/jobs']);
    }, error => {
      this.isLoading = false;
      this.errors = JSON.parse(error._body).errors;
    });
  }

}
