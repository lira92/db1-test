import { Component, OnInit } from '@angular/core';
import { JobsService } from '../../../services/jobs.service';
import * as moment from 'moment';

@Component({
  selector: 'app-jobs-list',
  templateUrl: './jobs-list.component.html',
  styleUrls: ['./jobs-list.component.css']
})
export class JobsListComponent implements OnInit {

  public jobs: any[] = [];
  public errors: any[] = [];
  public success: string;
  constructor(private service:JobsService) { }

  ngOnInit() {
    this.getJobs();
  }

  getJobs() {
    this.service.getJobs().subscribe(result => {
      this.jobs = result.data;
    })
  }

  formatDate(date: any) {
    if(!date) {
      return '';
    }
    return moment(date).format('DD/MM/YYYY HH:mm');
  }

  removeJob(id: any) {
    let confirm = window.confirm("Tem certeza que deseja remover essa vaga?");
    if(confirm) {
      this.service.removeJob(id).subscribe(result => {
        if(result.success) {
          this.success = "Vaga removida com sucesso";
          this.getJobs();
          return;
        }
        this.errors = result.errors;
      });
    }
  }
}
