import { Component, OnInit } from '@angular/core';
import { JobsService } from '../../../services/jobs.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-jobs-edit',
  templateUrl: './jobs-edit.component.html',
  styleUrls: ['./jobs-edit.component.css']
})
export class JobsEditComponent implements OnInit {

  public jobId: any;
  public form: FormGroup;
  public errors: any[] = [];
  private isLoading: Boolean = false;

  constructor(private fb: FormBuilder, private router: Router, private service: JobsService, private route: ActivatedRoute) { 
    this.form = this.fb.group({
      name: ['', Validators.compose([
        Validators.minLength(5),
        Validators.maxLength(100),
        Validators.required
      ])],
      description: ['', Validators.compose([
        
      ])]
    });
  }

  ngOnInit() {
    this.route.params.subscribe(
      (params: any) => {
        this.jobId = params['id'];
        this.getJob();
      }
    );
  }

  getJob() {
    this.service.getJob(this.jobId).subscribe(result => {
      this.form.setValue({
        name: result.data['name'],
        description: result.data['description']
      });
    }, error => {
      this.errors = [{message: 'Não foi possível recuperar as informações da vaga'}];
    });
  }

  submit() {
    this.isLoading = true;
    this.errors = [];
    this.service.updateJob(this.form.value, this.jobId).subscribe(result => {
      this.isLoading = false;
      this.form.reset();
      this.router.navigate(['/jobs']);
    }, error => {
      this.isLoading = false;
      this.errors = JSON.parse(error._body).errors;
    });
  }

}
