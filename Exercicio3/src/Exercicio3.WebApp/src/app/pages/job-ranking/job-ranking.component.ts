import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { JobsService } from '../../services/jobs.service';
import { TechnologiesService } from '../../services/technologies.service';
import { JobRankingService } from '../../services/job-ranking.service';
import { min } from 'rxjs/operators';

@Component({
  selector: 'app-job-ranking',
  templateUrl: './job-ranking.component.html',
  styleUrls: ['./job-ranking.component.css']
})
export class JobRankingComponent implements OnInit {

  public form: FormGroup;
  public errors: any[] = [];
  private isLoading: Boolean = false;
  public jobs: any[] = [];
  public technologies: any[] = [];
  public success: string = "";
  public result: any;

  constructor(private fb: FormBuilder, private router: Router, private jobsService: JobsService, 
      private technologiesService:TechnologiesService, private service:JobRankingService) {
    this.form = this.fb.group({
      jobId: ['', Validators.compose([
        Validators.required
      ])],
      technologies: this.buildTechnologies()
    });
   }

  ngOnInit() {
    this.getJobs();
    this.getTechnologies();
  }

  getJobs() {
    this.jobsService.getJobs().subscribe(result => {
      this.jobs = result.data;
    })
  }

  getTechnologies() {
    this.technologiesService.getTechnologies().subscribe(result => {
      this.technologies = result.data;
      this.form = this.fb.group({
        jobId: ['', Validators.compose([
          Validators.required
        ])],
        technologies: this.buildTechnologies()
      });
    })
  }

  buildTechnologies() {
    const arr = this.technologies.map(skill => {
      return this.fb.control("", Validators.compose([
        Validators.min(0)
      ]));
    });
    return this.fb.array(arr);
  }

  get technologiesFields() {
    return this.form.get('technologies');
  };

  submit() {
    this.success = "";
    const formValue = Object.assign({}, this.form.value, {
      technologies: this.form.value.technologies
        .map((value, i) => {
          return {
            technologyId: this.technologies[i].id,
            value: value
          }
        })
        .filter((item) => {
          return item.value != "";
        })
    });
    this.isLoading = true;
    this.errors = [];
    this.service.generateRanking(formValue).subscribe(result => {
      this.isLoading = false;
      this.form.reset();
      this.result = result.data;
    }, error => {
      this.isLoading = false;
      this.errors = JSON.parse(error._body).errors;
    });
  }

}
