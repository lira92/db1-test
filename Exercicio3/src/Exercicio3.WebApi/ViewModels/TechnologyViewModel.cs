﻿using System;

namespace Exercicio3.WebApi.ViewModels
{
    public class TechnologyViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}
