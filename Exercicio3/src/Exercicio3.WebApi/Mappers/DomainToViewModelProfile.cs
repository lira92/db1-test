﻿using AutoMapper;
using Exercicio3.Domain.Entities;
using Exercicio3.WebApi.ViewModels;

namespace Exercicio3.WebApi.Mappers
{
    public class DomainToViewModelProfile: Profile
    {
        public DomainToViewModelProfile()
        {
            CreateMap<Job, JobViewModel>();
            CreateMap<Technology, TechnologyViewModel>();
        }
    }
}
