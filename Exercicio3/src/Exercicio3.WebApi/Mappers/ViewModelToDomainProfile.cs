﻿using AutoMapper;
using Exercicio3.Domain.Entities;
using Exercicio3.WebApi.ViewModels;

namespace Exercicio3.WebApi.Mappers
{
    public class ViewModelToDomainProfile: Profile
    {
        public ViewModelToDomainProfile()
        {
            CreateMap<JobViewModel, Job>().ConstructUsing(x => new Job(x.Name, x.Description))
                .ForMember(e => e.Id, opt => opt.Ignore())
                .ForMember(e => e.CreatedAt, opt => opt.Ignore());
            CreateMap<TechnologyViewModel, Technology>().ConstructUsing(x => new Technology(x.Name))
                .ForMember(e => e.Id, opt => opt.Ignore())
                .ForMember(e => e.CreatedAt, opt => opt.Ignore());
        }
    }
}
