﻿using Exercicio3.Infra.Transaction;
using Flunt.Notifications;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exercicio3.WebApi.Controllers
{
    public class BaseController:Controller
    {
        private readonly IUow _uow;
        private readonly ILogger<BaseController> _logger;

        public BaseController(IUow uow, ILogger<BaseController> logger)
        {
            _uow = uow;
            _logger = logger;
        }

        public async Task<IActionResult> DefaultResponse(object result, IEnumerable<Notification> notifications)
        {
            if (notifications != null && !notifications.Any())
            {
                try
                {
                    await _uow.CommitAsync();
                    if (result == null)
                    {
                        return Ok(new
                        {
                            success = true
                        });
                    }
                    return Ok(new
                    {
                        success = true,
                        data = result
                    });
                }
                catch (Exception erro)
                {
                    await _uow.RollbackAsync();
                    _logger.LogError(erro, "Error on commit transaction");
                    return BadRequest(new
                    {
                        success = false,
                        errors = new[] { "Ocorreu uma falha interna no Servidor." }
                    });
                    throw;
                }
            }
            else
            {
                return BadRequest(new
                {
                    success = false,
                    errors = notifications
                });
            }
        }
    }
}
