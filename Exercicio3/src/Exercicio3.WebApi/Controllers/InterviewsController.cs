﻿using Exercicio3.Domain.Commands.Handlers;
using Exercicio3.Domain.Commands.Inputs;
using Exercicio3.Infra.Transaction;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Exercicio3.WebApi.Controllers
{
    public class InterviewsController : BaseController
    {
        private readonly InterviewCommandHandler _handler;
        public InterviewsController(IUow uow, ILogger<BaseController> logger, InterviewCommandHandler handler) : base(uow, logger)
        {
            _handler = handler;
        }

        [HttpPost]
        [Route("v1/interviews")]
        public Task<IActionResult> Create([FromBody]InterviewCommand command)
        {
            var result = _handler.Handle(command);
            return DefaultResponse(result, _handler.Notifications);
        }
    }
}
