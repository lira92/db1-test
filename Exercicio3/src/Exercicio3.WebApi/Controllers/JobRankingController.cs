﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exercicio3.Domain.Commands.Handlers;
using Exercicio3.Domain.Commands.Inputs;
using Exercicio3.Infra.Transaction;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Exercicio3.WebApi.Controllers
{
    public class JobRankingController : BaseController
    {
        private readonly JobRankingCommandHandler _handler;
        public JobRankingController(IUow uow, ILogger<BaseController> logger, JobRankingCommandHandler handler) : base(uow, logger)
        {
            _handler = handler;
        }

        [HttpPost]
        [Route("v1/job-ranking")]
        public Task<IActionResult> GetRanking([FromBody]JobRankingCommand command)
        {
            var result = _handler.Handle(command);
            return DefaultResponse(result, _handler.Notifications);
        }
    }
}
