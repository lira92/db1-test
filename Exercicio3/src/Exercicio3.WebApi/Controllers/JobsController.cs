﻿using AutoMapper;
using Exercicio3.Domain.Entities;
using Exercicio3.Domain.Repositories;
using Exercicio3.Infra.Transaction;
using Exercicio3.WebApi.ViewModels;
using Flunt.Notifications;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Exercicio3.WebApi.Controllers
{
    public class JobsController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly IJobRepository _jobRepository;
        public JobsController(IUow uow, ILogger<BaseController> logger, IMapper mapper, IJobRepository jobRepository) : base(uow, logger)
        {
            _mapper = mapper;
            _jobRepository = jobRepository;
        }

        [HttpGet]
        [Route("v1/jobs")]
        public Task<IActionResult> ListJobs()
        {
            var jobs = _jobRepository.GetAll();

            return DefaultResponse(_mapper.Map<IEnumerable<JobViewModel>>(jobs), new List<Notification>() { });
        }

        [HttpGet]
        [Route("v1/jobs/{id}")]
        public Task<IActionResult> GetJob(Guid id)
        {
            var job = _jobRepository.Get(id);

            if(job == null)
            {
                return DefaultResponse(null, new List<Notification>() { new Notification("job", "Esta vaga não existe.") });
            }

            return DefaultResponse(_mapper.Map<JobViewModel>(job), new List<Notification>() { });
        }

        [HttpPost]
        [Route("v1/jobs")]
        public Task<IActionResult> CreateJob([FromBody]JobViewModel viewModel)
        {
            var entity = _mapper.Map<Job>(viewModel);

            if(entity.Invalid)
            {
                return DefaultResponse(null, entity.Notifications);
            }

            _jobRepository.Create(entity);

            return DefaultResponse(new
            {
                id = entity.Id
            }, entity.Notifications);
        }

        [HttpPut]
        [Route("v1/jobs/{id}")]
        public Task<IActionResult> UpdateJob(Guid id, [FromBody]JobViewModel viewModel)
        {
            var entity = _jobRepository.Get(id);

            if (entity == null)
            {
                return DefaultResponse(null, new List<Notification>() { new Notification("job", "Esta vaga não existe.") });
            }

            entity.Update(viewModel.Name, viewModel.Description);

            if(entity.Invalid)
            {
                return DefaultResponse(null, entity.Notifications);
            }

            _jobRepository.Update(entity);

            return DefaultResponse(null, entity.Notifications);
        }

        [HttpDelete]
        [Route("v1/jobs/{id}")]
        public Task<IActionResult> RemoveJob(Guid id)
        {
            var entity = _jobRepository.Get(id);

            if (entity == null)
            {
                return DefaultResponse(null, new List<Notification>() { new Notification("job", "Esta vaga não existe.") });
            }

            _jobRepository.Remove(entity);

            return DefaultResponse(null, new List<Notification>() { });
        }
    }
}
