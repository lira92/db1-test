﻿using AutoMapper;
using Exercicio3.Domain.Entities;
using Exercicio3.Domain.Repositories;
using Exercicio3.Infra.Transaction;
using Exercicio3.WebApi.ViewModels;
using Flunt.Notifications;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Exercicio3.WebApi.Controllers
{
    public class TechnologiesController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly ITechnologyRepository _technologyRepository;
        public TechnologiesController(IUow uow, ILogger<BaseController> logger, IMapper mapper, ITechnologyRepository technologyRepository) : base(uow, logger)
        {
            _mapper = mapper;
            _technologyRepository = technologyRepository;
        }

        [HttpGet]
        [Route("v1/technologies")]
        public Task<IActionResult> ListTechnologies()
        {
            var technologies = _technologyRepository.GetAll();

            return DefaultResponse(_mapper.Map<IEnumerable<TechnologyViewModel>>(technologies), new List<Notification>() { });
        }

        [HttpGet]
        [Route("v1/technologies/{id}")]
        public Task<IActionResult> GetTechnology(Guid id)
        {
            var technology = _technologyRepository.Get(id);

            if (technology == null)
            {
                return DefaultResponse(null, new List<Notification>() { new Notification("technology", "Tecnologia não encontrada") });
            }

            return DefaultResponse(_mapper.Map<TechnologyViewModel>(technology), new List<Notification>() { });
        }

        [HttpPost]
        [Route("v1/technologies")]
        public Task<IActionResult> CreateTechnology([FromBody]TechnologyViewModel viewModel)
        {
            var entity = _mapper.Map<Technology>(viewModel);

            if (entity.Invalid)
            {
                return DefaultResponse(null, entity.Notifications);
            }

            _technologyRepository.Create(entity);

            return DefaultResponse(new
            {
                id = entity.Id
            }, entity.Notifications);
        }

        [HttpPut]
        [Route("v1/technologies/{id}")]
        public Task<IActionResult> UpdateTechnology(Guid id, [FromBody]TechnologyViewModel viewModel)
        {
            var entity = _technologyRepository.Get(id);

            if (entity == null)
            {
                return DefaultResponse(null, new List<Notification>() { new Notification("techonology", "Tecnologia não encontrada") });
            }

            entity.Update(viewModel.Name);

            if (entity.Invalid)
            {
                return DefaultResponse(null, entity.Notifications);
            }

            _technologyRepository.Update(entity);

            return DefaultResponse(null, entity.Notifications);
        }

        [HttpDelete]
        [Route("v1/technologies/{id}")]
        public Task<IActionResult> RemoveTechnology(Guid id)
        {
            var entity = _technologyRepository.Get(id);

            if (entity == null)
            {
                return DefaultResponse(null, new List<Notification>() { new Notification("techonology", "Tecnologia não encontrada") });
            }

            _technologyRepository.Remove(entity);

            return DefaultResponse(null, new List<Notification>() { });
        }
    }
}
