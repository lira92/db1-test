﻿using AutoMapper;
using Exercicio3.Domain.Commands.Handlers;
using Exercicio3.Domain.Repositories;
using Exercicio3.Infra.DataContext;
using Exercicio3.Infra.Repositories;
using Exercicio3.Infra.Transaction;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Exercicio3.WebApi
{
    public class Startup
    {
        public IHostingEnvironment HostingEnvironment { get; }
        public IConfiguration Configuration { get; }


        public Startup(IHostingEnvironment env, IConfiguration configmeters)
        {
            HostingEnvironment = env;
            Configuration = configmeters;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddAutoMapper();
            services.AddCors();

            // Context
            services.AddScoped<Exercicio3Context>();

            // Unit of Work
            services.AddTransient<IUow, Uow>();

            // Repositories
            services.AddTransient<ITechnologyRepository, TechnologyRepository>();
            services.AddTransient<IJobRepository, JobRepository>();
            services.AddTransient<ICandidateRepository, CandidateRepository>();

            // Command Handlers
            services.AddTransient<InterviewCommandHandler>();
            services.AddTransient<JobRankingCommandHandler>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(x =>
            {
                x.AllowAnyHeader();
                x.AllowAnyMethod();
                x.AllowAnyOrigin();
            });

            app.UseMvc();
        }
    }
}
