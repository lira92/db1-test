﻿using Exercicio3.Infra.DataContext;
using System.Threading.Tasks;

namespace Exercicio3.Infra.Transaction
{
    public class Uow : IUow
    {
        private readonly Exercicio3Context _context;

        public Uow(Exercicio3Context context)
        {
            _context = context;
        }

        public Task CommitAsync()
        {
            return _context.SaveChangesAsync();
        }

        public Task RollbackAsync()
        {
            // do nothing.
            return Task.CompletedTask;
        }
    }
}
