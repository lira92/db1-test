﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Exercicio3.Infra.Transaction
{
    public interface IUow
    {
        Task CommitAsync();
        Task RollbackAsync();
    }
}
