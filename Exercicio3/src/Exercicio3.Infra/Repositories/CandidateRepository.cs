﻿using Exercicio3.Domain.Entities;
using Exercicio3.Domain.Repositories;
using Exercicio3.Infra.DataContext;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Exercicio3.Infra.Repositories
{
    public class CandidateRepository : ICandidateRepository
    {
        private readonly Exercicio3Context _context;

        public CandidateRepository(Exercicio3Context context)
        {
            _context = context;
        }

        public void Create(Candidate candidate)
        {
            _context.Candidates.Add(candidate);
        }

        public Candidate GetCandidate(Guid id)
        {
            return _context.Candidates
                .Include(x => x.Email)
                .Include(x => x.CandidateTechnologies)
                .FirstOrDefault(x => x.Id == id);
        }

        public List<Candidate> GetCandidatesByJob(Guid jobId)
        {
            return _context.Candidates
                .Where(x => x.Job.Id == jobId)
                .Include(x => x.Email)
                .Include(x => x.CandidateTechnologies)
                    .ThenInclude(candidateTechnology => candidateTechnology.Technology)
                .ToList();
        }
    }
}
