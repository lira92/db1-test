﻿using Exercicio3.Domain.Entities;
using Exercicio3.Domain.Repositories;
using Exercicio3.Infra.DataContext;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exercicio3.Infra.Repositories
{
    public class JobRepository : IJobRepository
    {
        private readonly Exercicio3Context _context;

        public JobRepository(Exercicio3Context context)
        {
            _context = context;
        }

        public void Create(Job job)
        {
            _context.Jobs.Add(job);
        }

        public Job Get(Guid id)
        {
            return _context.Jobs.FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<Job> GetAll()
        {
            return _context.Jobs.ToList();
        }

        public void Remove(Job job)
        {
            _context.Jobs.Remove(job);
        }

        public void Update(Job job)
        {
            _context.Entry(job).State = EntityState.Modified;
        }
    }
}
