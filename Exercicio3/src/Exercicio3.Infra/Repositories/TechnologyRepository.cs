﻿using Exercicio3.Domain.Entities;
using Exercicio3.Domain.Repositories;
using Exercicio3.Infra.DataContext;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Exercicio3.Infra.Repositories
{
    public class TechnologyRepository: ITechnologyRepository
    {
        private readonly Exercicio3Context _context;

        public TechnologyRepository(Exercicio3Context context)
        {
            _context = context;
        }

        public void Create(Technology technology)
        {
            _context.Technologies.Add(technology);
        }

        public Technology Get(Guid id)
        {
            return _context.Technologies.FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<Technology> GetAll()
        {
            return _context.Technologies.ToList();
        }

        public void Remove(Technology technology)
        {
            _context.Technologies.Remove(technology);
        }

        public void Update(Technology technology)
        {
            _context.Entry(technology).State = EntityState.Modified;
        }
    }
}
