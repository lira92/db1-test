﻿using Exercicio3.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Exercicio3.Infra.Mappings
{
    public class CandidateMap : IEntityTypeConfiguration<Candidate>
    {
        public void Configure(EntityTypeBuilder<Candidate> builder)
        {
            builder.ToTable("Candidates");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Name).HasMaxLength(100).IsRequired();
            builder.OwnsOne(x => x.Email).Property(x => x.Address).HasMaxLength(255).IsRequired();
            builder.Property(x => x.CreatedAt).IsRequired();

            builder.HasOne(x => x.Job).WithMany().IsRequired();
            builder.HasMany(x => x.CandidateTechnologies).WithOne(x => x.Candidate).IsRequired();
        }
    }
}
