﻿using Exercicio3.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Exercicio3.Infra.Mappings
{
    public class CandidateTechnologyMap : IEntityTypeConfiguration<CandidateTechnology>
    {
        public void Configure(EntityTypeBuilder<CandidateTechnology> builder)
        {
            builder.ToTable("CandidatesTecnologies");
            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.Technology).WithMany().IsRequired();
        }
    }
}
