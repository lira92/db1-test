﻿namespace Exercicio3.Domain.Core.Commands
{
    public interface ICommandHandler<T> where T: ICommand
    {
        ICommandResult Handle(T command); 
    }
}
