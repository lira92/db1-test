﻿using Flunt.Notifications;
using System;

namespace Exercicio3.Domain.Core.Models
{
    public abstract class Entity: Notifiable
    {
        public Entity()
        {
            Id = Guid.NewGuid();
        }

        public Guid Id { get; protected set; }
    }
}
