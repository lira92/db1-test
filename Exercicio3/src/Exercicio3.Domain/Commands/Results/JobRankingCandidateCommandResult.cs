﻿using Exercicio3.Domain.Core.Commands;
using System;

namespace Exercicio3.Domain.Commands.Results
{
    public class JobRankingCandidateCommandResult:ICommandResult
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public int Score { get; set; }
    }
}
