﻿using Exercicio3.Domain.Core.Commands;
using System.Collections.Generic;

namespace Exercicio3.Domain.Commands.Results
{
    public class JobRankingCommandResult:ICommandResult
    {
        public IList<JobRankingCandidateCommandResult> Candidates { get; set; }
    }
}
