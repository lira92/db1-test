﻿using Exercicio3.Domain.Core.Commands;
using System;

namespace Exercicio3.Domain.Commands.Results
{
    public class InterviewCommandResult:ICommandResult
    {
        public Guid CandidateId { get; set; }
    }
}
