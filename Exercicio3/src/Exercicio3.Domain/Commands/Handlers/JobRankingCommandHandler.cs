﻿using Exercicio3.Domain.Commands.Inputs;
using Exercicio3.Domain.Commands.Results;
using Exercicio3.Domain.Core.Commands;
using Exercicio3.Domain.Repositories;
using Flunt.Notifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exercicio3.Domain.Commands.Handlers
{
    public class JobRankingCommandHandler : Notifiable, ICommandHandler<JobRankingCommand>
    {
        private readonly ICandidateRepository _candidateRepository;

        public JobRankingCommandHandler(ICandidateRepository candidateRepository)
        {
            _candidateRepository = candidateRepository;
        }

        public ICommandResult Handle(JobRankingCommand command)
        {
            command.Validate();
            if(command.Invalid)
            {
                AddNotifications(command);
                return null;
            }
            var result = new JobRankingCommandResult
            {
                Candidates = new List<JobRankingCandidateCommandResult>()
            };
            var candidates = _candidateRepository.GetCandidatesByJob(command.JobId.Value);

            if(candidates.Count == 0)
            {
                AddNotification("job", "Esta vaga não possui candidatos");
            }
            if(!command.Technologies.Where(x => x.Value > 0).Any())
            {
                AddNotification("techonologies", "Ao menos uma tecnologia você deve pontuar");
            }

            if(Invalid)
            {
                return null;
            }

            foreach(var candidate in candidates)
            {
                int score = 0;
                foreach(var candidateTechnology in candidate.CandidateTechnologies)
                {
                    var technologyFromCommand = command.Technologies.Where(x => x.TechnologyId == candidateTechnology.Technology.Id).FirstOrDefault();

                    if(technologyFromCommand != null)
                    {
                        score += technologyFromCommand.Value;
                    }
                }

                var candidateScore = new JobRankingCandidateCommandResult()
                {
                    Id = candidate.Id,
                    Name = candidate.Name,
                    Email = candidate.Email.Address,
                    Score = score
                };
                result.Candidates.Add(candidateScore);
            }

            result.Candidates = result.Candidates.OrderByDescending(x => x.Score).ToList();

            return result;
        }
    }
}
