﻿using Exercicio3.Domain.Commands.Inputs;
using Exercicio3.Domain.Commands.Results;
using Exercicio3.Domain.Core.Commands;
using Exercicio3.Domain.Entities;
using Exercicio3.Domain.Repositories;
using Exercicio3.Domain.ValueObjects;
using Flunt.Notifications;

namespace Exercicio3.Domain.Commands.Handlers
{
    public class InterviewCommandHandler : Notifiable, ICommandHandler<InterviewCommand>
    {
        private readonly ITechnologyRepository _technologyRepository;
        private readonly IJobRepository _jobRepository;
        private readonly ICandidateRepository _candidateRepository;

        public InterviewCommandHandler(ITechnologyRepository technologyRepository, IJobRepository jobRepository, ICandidateRepository candidateRepository)
        {
            _technologyRepository = technologyRepository;
            _jobRepository = jobRepository;
            _candidateRepository = candidateRepository;
        }

        public ICommandResult Handle(InterviewCommand command)
        {
            var job = _jobRepository.Get(command.JobId);

            if(job == null)
            {
                AddNotification("job", "A vaga não foi encontrada");
                return null;
            }

            var email = new Email(command.Email);
            var candidate = new Candidate(command.Name, email, job);

            foreach(var techId in command.Technologies)
            {
                var technology = _technologyRepository.Get(techId);

                if(technology == null)
                {
                    AddNotification("technology", $"A tecnologia {techId} não foi encontrada");
                    continue;
                }

                var candidateTechnology = new CandidateTechnology(technology);
                candidate.AddCandidateTechnology(candidateTechnology);
            }

            AddNotifications(candidate);
            if(Invalid)
            {
                return null;
            }

            _candidateRepository.Create(candidate);

            return new InterviewCommandResult
            {
                CandidateId = candidate.Id
            };
        }
    }
}
