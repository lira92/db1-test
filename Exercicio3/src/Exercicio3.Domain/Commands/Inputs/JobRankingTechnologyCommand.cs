﻿using Exercicio3.Domain.Core.Commands;
using Flunt.Notifications;
using Flunt.Validations;
using System;

namespace Exercicio3.Domain.Commands.Inputs
{
    public class JobRankingTechnologyCommand: Notifiable, ICommand
    {
        public Guid? TechnologyId { get; set; }
        public int Value { get; set; }

        public void Validate()
        {
            AddNotifications(
                new Contract().Requires()
                    .IsNotNull(TechnologyId, "TechonologyId", "Informe a tecnologia")
                    .IsGreaterOrEqualsThan(Value, 0, "Value", "Informe um valor maior ou igual a 0")
            );
        }
    }
}
