﻿using Exercicio3.Domain.Core.Commands;
using Flunt.Notifications;
using Flunt.Validations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exercicio3.Domain.Commands.Inputs
{
    public class JobRankingCommand: Notifiable, ICommand
    {
        public IEnumerable<JobRankingTechnologyCommand> Technologies { get; set; }
        public Guid? JobId { get; set; }

        public void Validate()
        {
            AddNotifications(
                new Contract().Requires().IsNotNull(JobId, "JobId", "Informe a vaga")
            );
            foreach (var technologyCommand in Technologies)
            {
                technologyCommand.Validate();
                AddNotifications(technologyCommand);
            }
        }
    }
}
