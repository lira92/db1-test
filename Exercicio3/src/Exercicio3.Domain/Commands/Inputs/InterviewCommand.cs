﻿using Exercicio3.Domain.Core.Commands;
using System;
using System.Collections.Generic;

namespace Exercicio3.Domain.Commands.Inputs
{
    public class InterviewCommand: ICommand
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public Guid JobId { get; set; }
        public IEnumerable<Guid> Technologies { get; set; }
    }
}
