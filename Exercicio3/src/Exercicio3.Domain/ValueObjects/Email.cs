﻿using Exercicio3.Domain.Core.Models;
using Flunt.Validations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exercicio3.Domain.ValueObjects
{
    public class Email: ValueObject
    {
        protected Email() { }
        public Email(string address)
        {
            Address = address;

            AddNotifications(
                new Contract().Requires().IsEmail(Address, "Email", "E-mail inválido")
            );
        }

        public string Address { get; private set; }

        override public string ToString()
        {
            return Address;
        }
    }
}
