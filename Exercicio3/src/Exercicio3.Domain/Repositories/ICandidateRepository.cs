﻿using Exercicio3.Domain.Entities;
using System;
using System.Collections.Generic;

namespace Exercicio3.Domain.Repositories
{
    public interface ICandidateRepository
    {
        void Create(Candidate candidate);
        Candidate GetCandidate(Guid id);
        List<Candidate> GetCandidatesByJob(Guid jobId);
    }
}
