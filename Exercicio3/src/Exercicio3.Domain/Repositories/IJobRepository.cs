﻿using Exercicio3.Domain.Entities;
using System;
using System.Collections.Generic;

namespace Exercicio3.Domain.Repositories
{
    public interface IJobRepository
    {
        void Create(Job job);
        void Update(Job job);
        IEnumerable<Job> GetAll();
        Job Get(Guid id);
        void Remove(Job job);
    }
}
