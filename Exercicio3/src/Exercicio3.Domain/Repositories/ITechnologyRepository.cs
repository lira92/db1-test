﻿using Exercicio3.Domain.Entities;
using System;
using System.Collections.Generic;

namespace Exercicio3.Domain.Repositories
{
    public interface ITechnologyRepository
    {
        void Create(Technology techonology);
        void Update(Technology techonology);
        IEnumerable<Technology> GetAll();
        Technology Get(Guid id);
        void Remove(Technology techonology);
    }
}
