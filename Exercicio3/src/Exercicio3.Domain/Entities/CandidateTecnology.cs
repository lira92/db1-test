﻿using Exercicio3.Domain.Core.Models;
using Flunt.Validations;

namespace Exercicio3.Domain.Entities
{
    public class CandidateTechnology: Entity
    {
        protected CandidateTechnology()
        {

        }
        public CandidateTechnology(Technology technology)
        {
            Technology = technology;

            AddNotifications(
                new Contract().Requires().IsNotNull(Technology, "Techonology", "Informe a tecnologia")
            );
        }

        public Candidate Candidate { get; private set; }
        public Technology Technology { get; private set; }
    }
}
