﻿using Exercicio3.Domain.Core.Models;
using Flunt.Validations;
using System;

namespace Exercicio3.Domain.Entities
{
    public class Job: Entity
    {
        protected Job() { }
        public Job(string name, string description)
        {
            Name = name;
            Description = description;
            CreatedAt = DateTime.Now;

            Validate();
        }

        public string Name { get; private set; }
        public string Description { get; private set; }
        public DateTime CreatedAt { get; private set; }
        public DateTime? UpdatedAt { get; private set; }

        public void Update(string name, string description)
        {
            Name = name;
            Description = description;
            UpdatedAt = DateTime.Now;

            Validate();
        }

        private void Validate()
        {
            AddNotifications(
                new Contract().Requires()
                    .IsNotNullOrEmpty(Name, "Name", "Informe o nome da vaga")
                    .HasMaxLen(Name, 100, "Name", "O nome deve ter no máximo 100 caracteres")
                    .HasMinLen(Name, 5, "Name", "O nome da vaga deve ter no mínimo 5 caracteres")
            );
        }
    }
}
