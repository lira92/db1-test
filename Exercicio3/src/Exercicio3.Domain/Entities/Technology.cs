﻿using Exercicio3.Domain.Core.Models;
using Flunt.Validations;
using System;

namespace Exercicio3.Domain.Entities
{
    public class Technology:Entity
    {
        protected Technology() { }
        public Technology(string name)
        {
            Name = name;
            CreatedAt = DateTime.Now;

            Validate();
        }

        public string Name { get; private set; }
        public DateTime CreatedAt { get; private set; }
        public DateTime? UpdatedAt { get; private set; }

        public void Update(string name)
        {
            Name = name;
            UpdatedAt = DateTime.Now;

            Validate();
        }

        private void Validate()
        {
            AddNotifications(
                new Contract().Requires()
                    .IsNotNullOrEmpty(Name, "Name", "Informe o nome da vaga")
                    .HasMaxLen(Name, 100, "Name", "O nome deve ter no máximo 100 caracteres")
            );
        }
    }
}
