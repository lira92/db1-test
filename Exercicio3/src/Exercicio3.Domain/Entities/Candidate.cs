﻿using Exercicio3.Domain.Core.Models;
using Exercicio3.Domain.ValueObjects;
using Flunt.Validations;
using System;
using System.Collections.Generic;

namespace Exercicio3.Domain.Entities
{
    public class Candidate: Entity
    {
        protected Candidate()
        {

        }
        public Candidate(string name, Email email, Job job)
        {
            Name = name;
            Email = email;
            CreatedAt = DateTime.Now;
            Job = job;
            CandidateTechnologies = new List<CandidateTechnology>();

            AddNotifications(
                new Contract().Requires()
                    .IsNotNull(Job, "Job", "Informe a vaga")
                    .IsNotNullOrEmpty(Name, "Name", "Informe o nome do candidato")
                    .HasMinLen(Name, 2, "Name", "Informe pelo meno 2 caracteres para o nome")
                    .HasMaxLen(Name, 100, "Name", "O nome deve conter no máximo 100 caracteres"),
                Email
            );
        }

        public string Name { get; private set; }
        public Email Email { get; private set; }
        public DateTime CreatedAt { get; private set; }
        public Job Job { get; private set; }
        public IList<CandidateTechnology> CandidateTechnologies { get; private set; }

        public void AddCandidateTechnology(CandidateTechnology candidateTechnology)
        {
            AddNotifications(candidateTechnology);
            if(candidateTechnology.Valid)
            {
                CandidateTechnologies.Add(candidateTechnology);
            }
        }
    }
}
