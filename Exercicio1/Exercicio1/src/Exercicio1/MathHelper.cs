﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exercicio1
{
    public class MathHelper
    {
        public static bool HasOnlyOddNumbers(int[] numbers)
        {
            return !numbers.Where(x => IsEven(x)).Any();
        }

        public static bool IsEven(int number)
        {
            return (number % 2) == 0;
        }
    }
}
