﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exercicio1
{
    public class Collatz
    {
        public static int[] Calculate(int n)
        {
            int iterator = n;
            List<int> sequence = new List<int>() { iterator };
            while (iterator > 1)
            {
                iterator = GetNextCollatzNumber(iterator);
                sequence.Add(iterator);
            }
            return sequence.ToArray();
        }
        
        public  static int DiscoverGreaterSequenceOwner(int initial, int final)
        {
            int greaterSequenceOwner = initial;
            int greaterSequenceLength = Collatz.Calculate(initial).Length;
            for (var i = initial + 1; i <= final; i++)
            {
                var sequence = Collatz.Calculate(i);
                if (sequence.Length > greaterSequenceLength)
                {
                    greaterSequenceLength = sequence.Length;
                    greaterSequenceOwner = i;
                }
            }

            return greaterSequenceOwner;
        }

        private static int GetNextCollatzNumber(int n)
        {
            if (MathHelper.IsEven(n))
            {
                return n / 2;
            }
            return (3 * n) + 1;
        }
    }
}
