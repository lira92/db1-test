﻿using System.Linq;

namespace Exercicio1
{
    public class MathematicalSets
    {
        public static int[] GetElementsNotInOtherArray(int[] firstArray, int[] secondArray)
        {
            var comumNumbers = firstArray.Intersect(secondArray).ToArray();
            return firstArray.Where(x => !comumNumbers.Contains(x)).ToArray();
        }
    }
}
