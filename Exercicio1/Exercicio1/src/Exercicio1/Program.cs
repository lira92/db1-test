﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Exercicio1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Calculando o número com a maior sequência entre 1 e 1.000.000...");
            int result = Collatz.DiscoverGreaterSequenceOwner(1, 1000000);
            Console.WriteLine($"O número é: {result}");
            Console.WriteLine("Verificando se o array: { 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144 } contém somente número ímpares...");
            bool hasOnlyOddNumbers = MathHelper.HasOnlyOddNumbers(new int[] { 1, 3, 5, 13, 21, 55, 89 });
            Console.WriteLine(hasOnlyOddNumbers ? "O array possui somente número ímpares" : "O array não contém somente números ímpares.");
            Console.WriteLine("Verificando elementos do primeiro array (1, 3, 7, 29, 42, 98, 234, 93) que não estejam no segundo (4, 6, 93, 7, 55, 32, 3)");
            int[] elements = MathematicalSets.GetElementsNotInOtherArray(new int[] { 1, 3, 7, 29, 42, 98, 234, 93 }, new int[] { 4, 6, 93, 7, 55, 32, 3 });
            Console.WriteLine($"Os elementos não presentes no segundo array são: {string.Join(", ", elements)}");
            Console.ReadKey();
        }
    }
}
