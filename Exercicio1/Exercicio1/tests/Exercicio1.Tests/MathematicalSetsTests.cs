﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exercicio1.Tests
{
    [TestClass]
    public class MathematicalSetsTests
    {
        [TestMethod]
        [DataRow(new int[] { 1, 2, 3 }, new int[] { 1, 3 }, new int[] { 2 })]
        [DataRow(new int[] { 1, 2, 3, 5, 10 }, new int[] { 1, 3 }, new int[] { 2, 5, 10 })]
        [DataRow(new int[] { 10, 8, 40, 53, 150 }, new int[] {  }, new int[] { 10, 8, 40, 53, 150 })]
        [DataRow(new int[] {  }, new int[] { 1, 3 }, new int[] {  })]
        public void TestGetElementsNotInOtherArray(int[] firstArray, int[] secondArray, int[] expected)
        {
            var result = MathematicalSets.GetElementsNotInOtherArray(firstArray, secondArray);
            CollectionAssert.AreEqual(expected, result);
        }
    }
}
