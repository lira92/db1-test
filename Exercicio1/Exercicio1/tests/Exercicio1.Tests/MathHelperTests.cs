﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exercicio1.Tests
{
    [TestClass]
    public class MathHelperTests
    {
        [TestMethod]
        public void TestHasOnlyOddNumbersRight()
        {
            var result = MathHelper.HasOnlyOddNumbers(new int[] { 1, 3, 5, 9, 17, 835 });
            Assert.IsTrue(result);
        }

        [TestMethod]
        [DataRow(new int[] { 1, 2, 3, 5, 9, 17, 835 })]
        [DataRow(new int[] { 2, 4, 6, 8, 10 })]
        public void TestHasOnlyOddNumbersWrong(int[] numbers)
        {
            var result = MathHelper.HasOnlyOddNumbers(numbers);
            Assert.IsFalse(result);
        }
    }
}
