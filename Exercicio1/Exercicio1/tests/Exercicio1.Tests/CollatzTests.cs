﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exercicio1.Tests
{
    [TestClass]
    public class CollatzTests
    {
        [TestMethod]
        public void TestCalculate()
        {
            var sequence = Collatz.Calculate(13);
            CollectionAssert.AreEqual(new int[] { 13, 40, 20, 10, 5, 16, 8, 4, 2, 1 }, sequence);
        }

        [TestMethod]
        public void TestDiscoverGreaterSequenceOwner()
        {
            var result = Collatz.DiscoverGreaterSequenceOwner(1, 10);
            Assert.AreEqual(9, result);
        }
    }
}
