import { Component, OnInit } from '@angular/core';
import { GithubService } from '../../services/github.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  public users:any[];
  constructor(private service:GithubService) { }

  ngOnInit() {
    this.service.getUsers().subscribe(result => {
      this.users = result;
    })
  }

  loadMore() {
    this.service.getNextUsers().subscribe(result => {
      this.users = this.users.concat(result);
    })
  }

}
