import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GithubService } from '../../services/github.service';
import * as moment from 'moment';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {
  public username: any;
  public user:any = {};
  public repositories: any[] = [];
  constructor(private route: ActivatedRoute, private service: GithubService) { }

  ngOnInit() {
    console.log(this.repositories);
    this.route.params.subscribe(
      (params: any) => {
        this.username = params['username'];
        this.getUser();
        this.getUserRepositories();
      }
    );

  }

  private getUser() {
    this.service.getUser(this.username).subscribe((response) => {
      this.user = response;
    });
  }

  private getUserRepositories() {
    this.service.getUserRepositories(this.username).subscribe((response) => {
      this.repositories = response;
      console.log(this.repositories);
    })
  }

  public loadMoreRepos() {
    this.service.getNextUserRepositories().subscribe((response) => {
      this.repositories = this.repositories.concat(response);
      console.log(this.repositories);
    })
  }

  public getFormattedCreatedAt()
  {
    return moment(this.user.created_at).format('DD/MM/YYYY hh:mm');
  }

}
