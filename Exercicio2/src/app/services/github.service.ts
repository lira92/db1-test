import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import parse from 'parse-link-header';

@Injectable({
  providedIn: 'root'
})
export class GithubService {

  private nextLink:string = "";
  private nextReposLink:string = "";
  constructor(private http:Http) { }

  getUsers() {
    return this.http.get(environment.api_base_url + 'users')
      .pipe(
        map((response:Response) => {
          let parsedLink = parse(response.headers.get('link'));
          this.nextLink = parsedLink.next.url;
          return response.json();
        })
      );
  }

  getNextUsers() {
    return this.http.get(this.nextLink)
      .pipe(
        map((response:Response) => {
          let parsedLink = parse(response.headers.get('link'));
          this.nextLink = parsedLink.next.url;
          return response.json();
        })
      );
  }

  getUser(username) {
    return this.http.get(environment.api_base_url + `users/${username}`).pipe(map((response:Response) => response.json()));
  }

  getUserRepositories(username) {
    return this.http.get(environment.api_base_url + `users/${username}/repos`)
      .pipe(
        map((response:Response) => {
          let parsedLink = parse(response.headers.get('link'));
          console.log(parsedLink);
          this.nextReposLink = parsedLink.next.url;
          return response.json()
        })
      );
  }

  getNextUserRepositories() {
    if(!this.nextReposLink) {
      return;
    }
    return this.http.get(this.nextReposLink)
      .pipe(
        map((response:Response) => {
          let parsedLink = parse(response.headers.get('link'));
          this.nextReposLink = "";
          if(parsedLink.next) {
            this.nextReposLink = parsedLink.next.url;
          }
          return response.json()
        })
      );
  }
}
