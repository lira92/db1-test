import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

//Root
import { AppComponent } from './app.component';

//Routes
import {Routing, RoutingProviders} from './app.routing';

//Pages
import { HomePageComponent } from './pages/home-page/home-page.component';

//Shared
import { HeadbarComponent } from './components/shared/headbar/headbar.component';

//Services
import { GithubService } from './services/github.service';
import { UserDetailComponent } from './pages/user-detail/user-detail.component';
import { FooterComponent } from './components/shared/footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    UserDetailComponent,
    HeadbarComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    Routing,
    HttpModule,
  ],
  providers: [GithubService],
  bootstrap: [AppComponent]
})
export class AppModule { }
